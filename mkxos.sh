#!/bin/bash
echo "Making Thomas's xos image..."
echo "Assembling dependencies..."
./xas kernel.xas kernel.xo
./xas hello.xas hello.xo
./xas yello.xas yello.xo
./xas zello.xas zello.xo
./xas goodbye.xas goodbye.xo
echo "Dependencies assembled."
echo "Generating image..."
./xmkos xos kernel.xo hello.xo yello.xo zello.xo goodbye.xo
echo "Image generated."
echo "Thomas's xos image created."

