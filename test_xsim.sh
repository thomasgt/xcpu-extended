#!/bin/bash
echo "Testing xsim..."
ticks=100000
quantum=500

echo "Building dependencies..."
make all

echo "Running tests..."
for i in {0..9}
do 
  echo "Running xsim test case: $i"
  asm_filename=$(printf 'test-xsim/test.sim.%02d.xas' "$i")
  obj_filename=$(printf 'test-xsim/test.sim.%02d.xo' "$i")
  tom_filename=$(printf 'test-xsim/test.sim.%02d.tom.out' "$i")
  gold_filename=$(printf 'test-xsim/test.sim.%02d.gold.out' "$i")
  diag_filename=$(printf 'test-xsim/test.sim.%02d.diag.out' "$i")
  ./xas $asm_filename $obj_filename > $diag_filename
  ./xsim_gold $ticks $obj_filename $quantum > $gold_filename
  ./xsim $ticks $obj_filename $quantum > $tom_filename
  diff -q -s $gold_filename $tom_filename
done

