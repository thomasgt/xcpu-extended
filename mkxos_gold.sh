#!/bin/bash
echo "Making the gold xos image..."
echo "Assembling dependencies..."
./xas hello.xas hello.xo
./xas yello.xas yello.xo
./xas zello.xas zello.xo
./xas goodbye.xas goodbye.xo
echo "Dependencies assembled."
echo "Generating image..."
./xmkos xos_gold kernel_gold.xo hello.xo yello.xo zello.xo goodbye.xo
echo "Image generated."
echo "Gold xos image created."

