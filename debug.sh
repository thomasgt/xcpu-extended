#!/bin/bash
echo "Running debug script..."
# Define variables
netTicks=10000
intrTicks=500
echo "Building dependencies..." 
# Call build scripts
make > test-data/diagnostics.out
./mkxos.sh > test-data/diagnostics.out
./mkxos_gold.sh > test-data/diagnostics.out
echo "Running golden kernel with Thomas's simulator..."
./xsim $netTicks xos_gold $intrTicks > test-data/thomas_gold_debug.out
echo "Running Thomas's kernel with Thomas's simulator..."
./xsim $netTicks xos $intrTicks > test-data/thomas_thomas_debug.out
echo "Test complete."
