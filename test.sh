#!/bin/bash
echo "Running test script..."
# Define variables
netTicks=10000
intrTicks=500
echo "Building dependencies..." 
# Call build scripts
make > test-data/diagnostics.out
./mkxos.sh > test-data/diagnostics.out
./mkxos_gold.sh > test-data/diagnostics.out
echo "Running golden kernel with golden simulator..."
./xsim_gold $netTicks xos_gold $intrTicks > test-data/gold_gold.out
echo "Running Thomas's kernel with golden simulator..."
./xsim_gold $netTicks xos $intrTicks > test-data/gold_thomas.out
echo "Running golden kernel with Thomas's simulator..."
./xsim $netTicks xos_gold $intrTicks > test-data/thomas_gold.out
echo "Running Thomas's kernel with Thomas's simulator..."
./xsim $netTicks xos $intrTicks > test-data/thomas_thomas.out
echo "Comparing results with diff..."
diff test-data/gold_gold.out test-data/thomas_thomas.out
echo "Test complete."
