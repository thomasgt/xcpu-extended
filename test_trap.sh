#!/bin/bash
echo "Testing interrupt handler..."
ticks=100000
quantum=1000

echo "Building dependencies..."
make all > test-trap/diagnostics.out

echo "Assembling dependencies..."
./xas hello.xas hello.xo > test-trap/diagnostics.out
./xas yello.xas yello.xo > test-trap/diagnostics.out
./xas zello.xas zello.xo > test-trap/diagnostics.out
./xas goodbye.xas goodbye.xo > test-trap/diagnostics.out
./xas kernel.xas kernel.xo > test-trap/diagnostics.out

echo "Generating image..."
./xmkos xos kernel.xo hello.xo yello.xo zello.xo goodbye.xo > test-trap/diagnostics.out

echo "Running test..."
./xsim $ticks xos $quantum > test-trap/test.trap.out

