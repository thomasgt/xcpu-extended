#!/bin/bash
echo "Testing interrupt handler..."
ticks=100000
quantum=1000

echo "Building dependencies..."
make all > test-intr/diagnostics.out

echo "Assembling dependencies..."
./xas test-intr/test.A.xas test-intr/test.A.xo > test-intr/diagnostics.out
./xas test-intr/test.B.xas test-intr/test.B.xo > test-intr/diagnostics.out
./xas kernel.xas kernel.xo > test-intr/diagnostics.out

echo "Generating image..."
./xmkos xos kernel.xo test-intr/test.A.xo test-intr/test.B.xo > test-intr/diagnostics.out

echo "Running test..."
./xsim $ticks xos $quantum > test-intr/test.intr.out

